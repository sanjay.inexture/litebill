import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DynamicExcelComponent } from './dynamic-excel/dynamic-excel.component';
import { SampleExcelComponent } from './sample-excel/sample-excel.component';
// import { MaterialFormComponent } from './Angular Material/material-form/material-form.component';

const routes: Routes = [
  {
    path: 'sample-excel',
    component: SampleExcelComponent,
  },
  {
    path: 'dynamic-excel',
    component: DynamicExcelComponent,
  },
  // {
  //   path: 'angular-material-form',
  //   component: MaterialFormComponent,
  // },
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

import { Component, HostListener, Input } from "@angular/core";
import * as XLSX from "xlsx";
import readXlsxFile from "read-excel-file";
import { HttpClient } from "@angular/common/http";
import { FormBuilder, Validators } from "@angular/forms";
import * as FileSaver from "file-saver";
import { LiteBillService } from "./services/lite-bill.service";
import { Subject } from "rxjs";
declare var $: any;
import * as _ from "lodash";
import liteBill from "./Model/liteBill";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"],
})
export class AppComponent {
  _: any = _;
  objectKeys = Object.keys;
  title = "angular";
  colums: any = [];
  // create schema for filed name with type
  schema = {
    Date: {
      prop: "Date",
      type: Date,
    },
    Current_Unit: {
      prop: "Current_Unit",
      type: String,
    },
    Extra_Current_Unit: {
      prop: "Extra_Current_Unit",
      type: String,
    },
    Previouse_Unit: {
      prop: "Previouse_Unit",
      type: String,
    },
    Extra_Previouse_Unit: {
      prop: "Extra_Previouse_Unit",
      type: String,
    },
    Used_Unit: {
      prop: "Used_Unit",
      type: String,
    },
    Extra_Used_Unit: {
      prop: "Extra_Used_Unit",
      type: String,
    },
    Unit_Point: {
      prop: "Unit_Point",
      type: String,
    },
    RS: {
      prop: "RS",
      type: String,
    },
    comments: {
      prop: "comments",
      type: String,
    },
  };
  showAddForm: any = false;
  lastLiteBillData = new liteBill();
  newLastBill = new liteBill();
  liteBillForm: any;
  data: any = [];
  unit_point = 8;
  fileName: string = "liteBill.xlsx";
  showError = false;
  new_unit_bill: any;
  isDataLoaded = true;
  storeData: any;
  csvData: any;
  jsonData: any;
  textData: any;
  htmlData: any;
  fileUploaded: any;
  worksheet: any;
  workBook: any;
  selectedWorkSheet: any;
  worksSheets: any;
  selectedWorkSheetName: any;
  downloadViewOpen = false;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject<any>();
  activePage: number = 0;
  page: number = 1;
  totalRecords: any;
  pages: any;
  currentPage: any;
  displayData: any;
  minSize: any;
  maxSize: any;
  selectedWorkSheetNamesArray: any;
  convertedData: any;
  copyData: any;
  isOpenAddMenu = false;
  new_subscraction: any = 0;
  new_addition: any = 0;
  displayActivePage(activePageNumber: number) {
    this.activePage = activePageNumber;
  }
  constructor(
    public formBuilder: FormBuilder,
    public liteBillService: LiteBillService
  ) {}

  ngOnInit(): void {
    this.currentPage = this.page;
    //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
    //Add 'implements OnInit' to the class.
    // this.liteBillForm = this.formBuilder.group({
    //   current_unit: ["", Validators.required],
    //   previouse_unit: ["", Validators.required],
    //   Used_Unit: ["", Validators.required],
    //   Extra_Current_Unit: ["", Validators.required],
    //   Extra_Previouse_Unit: ["", Validators.required],
    //   Extra_Used_Unit: ["", Validators.required],
    //   Unit_Point: ["", Validators.required],
    //   RS: ["", Validators.required],
    //   comments: ["", Validators.required],
    // });
    // let that = this;
    // $(document).ready(function () {
    //   $('[data-toggle="tooltip"]').tooltip();
    //   var actions = $("table td:last-child").html();
    //   // Append table with add row form on add new button click
    //   $(".add-new").click(function () {
    //     $(that).attr("disabled", "disabled");
    //     var index = $("table tbody tr:last-child").index();
    //     var row =
    //       "<tr>" +
    //       '<td><input type="text" class="form-control" name="name" id="name"></td>' +
    //       '<td><input type="text" class="form-control" name="department" id="department"></td>' +
    //       '<td><input type="text" class="form-control" name="phone" id="phone"></td>' +
    //       "<td>" +
    //       actions +
    //       "</td>" +
    //       "</tr>";
    //     $("table").append(row);
    //     $("table tbody tr")
    //       .eq(index + 1)
    //       .find(".add, .edit")
    //       .toggle();
    //     $('[data-toggle="tooltip"]').tooltip();
    //   });
    //   // Add row on add button click
    //   $(document).on("click", ".add", function () {
    //     var empty = false;
    //     var input = $(that).parents("tr").find('input[type="text"]');
    //     input.each(function () {
    //       if (!$(that).val()) {
    //         $(that).addClass("error");
    //         empty = true;
    //       } else {
    //         $(that).removeClass("error");
    //       }
    //     });
    //     $(that).parents("tr").find(".error").first().focus();
    //     if (!empty) {
    //       input.each(function () {
    //         $(that).parent("td").html($(that).val());
    //       });
    //       $(that).parents("tr").find(".add, .edit").toggle();
    //       $(".add-new").removeAttr("disabled");
    //     }
    //   });
    //   // Edit row on edit button click
    //   $(document).on("click", ".edit", function () {
    //     $(that)
    //       .parents("tr")
    //       .find("td:not(:last-child)")
    //       .each(function () {
    //         $(that).html(
    //           '<input type="text" class="form-control" value="' +
    //             $(that).text() +
    //             '">'
    //         );
    //       });
    //     $(that).parents("tr").find(".add, .edit").toggle();
    //     $(".add-new").attr("disabled", "disabled");
    //   });
    //   // Delete row on delete button click
    //   $(document).on("click", ".delete", function () {
    //     $(that).parents("tr").remove();
    //     $(".add-new").removeAttr("disabled");
    //   });
    // });
  }

  reset = () => {
    // let copyData = this.newLastBill as any;
    let convertedObj = {
      Current_Unit: "0",
      Previouse_Unit: "0",
      Extra_Unit: "0",
      Used_Unit: "0",
      Unit_Point: "0",
      RS: "",
      Date: "",
      Comments: "",
    } as any;
    return convertedObj as any;
  };

  /**
   * get selected excel file data
   * @param evt current tag event
   */
  onFileChange(evt: any) {
    this.isDataLoaded = false;
    this.data = [];
    this.liteBillService
      .getSelectedFileData(evt)
      .then((res: any) => {
        if (JSON.stringify(res) != "{}") {
          // let {workBook,response} = res;
          let {
            selectedWorkSheet,
            selectedWorkSheetName,
            workBook,
            convertedData,
            selectedWorkSheetNamesArray,
            worksSheets,
            colums,
            lastLiteBillData,
          } = res;
          // debugger;
          this.selectedWorkSheet = selectedWorkSheet;
          this.selectedWorkSheetName = selectedWorkSheetName;
          this.workBook = workBook;
          this.convertedData = convertedData;
          this.selectedWorkSheetNamesArray = selectedWorkSheetNamesArray;
          this.worksSheets = worksSheets;
          this.colums = colums;
          this.lastLiteBillData = lastLiteBillData;
          this.currentPage = 1;
          this.page = 1;
          this.arrangeData(convertedData);
          this.isDataLoaded = true;
          // this.setData(workBook, response);
        }
      })
      .catch((err) => {
        this.isDataLoaded = true;
        throw new Error(err.message);
      });
  }

  openAddMenu() {
    if (this.isOpenAddMenu) {
      this.isOpenAddMenu = false;
      return;
    }
    var today = new Date();
    // var dd = String(today.getDate()).padStart(2, "0");
    // var mm = String(today.getMonth() + 1).padStart(2, "0"); //January is 0!
    // var yyyy = today.getFullYear();

    this.newLastBill.Date = today;
    this.newLastBill.Previouse_Unit = this.lastLiteBillData.Current_Unit;
    this.isOpenAddMenu = true;
  }

  updateData() {
    let new_unit = 0;
    if (this.newLastBill.Current_Unit) {
      new_unit = this.newLastBill.Current_Unit;
      this.showError = false;
      if (
        JSON.stringify(this.lastLiteBillData) === "{}" ||
        JSON.stringify(this.lastLiteBillData) === "[]"
      ) {
        // do substraction with new and old unit
        let substraction = parseFloat(new_unit.toString()) - 0;
        // do multiplication with subscraction and unit point unit
        let answer =
          parseFloat(substraction.toString()) *
          parseFloat(this.unit_point.toString());
        this.new_unit_bill = answer.toFixed(2);
        console.log(answer);
      } else {
        this.new_addition = 0;
        this.newLastBill = this.convetFloat();
        // this.newLastBill.Current_Unit = this.convetFloat(
        //   this.newLastBill.Current_Unit
        // );
        // this.newLastBill.Extra_Unit = this.convetFloat(
        //   this.newLastBill.Extra_Unit
        // );
        // this.newLastBill.Previouse_Unit = this.convetFloat(
        //   this.newLastBill.Previouse_Unit
        // );
        // this.newLastBill.Used_Unit = this.convetFloat(
        //   this.newLastBill.Used_Unit
        // );
        // this.newLastBill.RS = this.convetFloat(this.newLastBill.RS);
        this.new_addition =
          parseFloat(this.newLastBill.Current_Unit) +
          parseFloat(this.newLastBill.Extra_Unit);
        this.new_subscraction =
          this.new_addition - this.newLastBill.Previouse_Unit;
        this.newLastBill.Used_Unit = this.new_subscraction;
        this.newLastBill.RS =
          this.newLastBill.Unit_Point * this.newLastBill.Used_Unit;
        this.newLastBill = this.convertToFix();
        // this.newLastBill.Current_Unit = this.convertToFix(this.)
        console.log("this.newLastBill ==>", this.newLastBill);
        // let data = this.lastLiteBillData;
        // if (
        //   !(parseFloat(new_unit.toString()) > parseFloat(data.Current_Unit))
        // ) {
        //   this.showError = true;
        //   return;
        // }
        // // do substraction with new and old unit
        // let substraction =
        //   parseFloat(new_unit.toString()) - parseFloat(data.Current_Unit);
        // // do multiplication with subscraction and unit point unit
        // let answer =
        //   parseFloat(substraction.toString()) * parseFloat(data.Unit_Point);
        // this.new_unit_bill = answer.toFixed(2);
        // console.log(answer);
        // let date = new Date();
        // let previouse_unit = this.lastLiteBillData.Current_Unit;
        // let Unit_Point = this.lastLiteBillData.Unit_Point;
        // let postObj = [
        //   date,
        //   this.newLastBill.Current_Unit,
        //   previouse_unit,
        //   substraction,
        //   Unit_Point,
        //   answer,
        //   "",
        // ];
        // console.log("post ob ==>", postObj);
      }
    }
  }

  /**
   *
   * @param value
   * @returns convert float to string
   */
  convetFloat() {
    let copyData = this.newLastBill as any;
    let convertedObj = {} as any;
    Object.keys(copyData).filter((x: any) => {
      let value = copyData[x];
      if (x === "Date" || x === "Comments") {
        convertedObj[x] = value;
        return;
      }
      if (!value || value === "") {
        value = "0";
      }
      // console.log((Math.round(x * 100) / 100).toFixed(2));
      if (typeof value === "string") {
        value = parseFloat(value);
      }
      convertedObj[x] = value;
      // this.newLastBill = value;
    });
    return convertedObj as any;
  }

  /**
   *
   * @param value
   * @returns convert to fixed value
   */
  convertToFix() {
    let copyData = this.newLastBill as any;
    let convertedObj = {} as any;
    Object.keys(copyData).filter((x: any) => {
      let value = copyData[x];
      if (x === "Date" || x === "Comments" || x === "Unit_Point") {
        convertedObj[x] = value;
        return;
      }
      if (!value || value === "") {
        value = "0";
      }
      // console.log((Math.round(x * 100) / 100).toFixed(2));
      value = (Math.round(value * 100) / 100).toFixed(2);
      convertedObj[x] = value;
    });
    return convertedObj as any;
    // return (Math.round(value * 100) / 100).toFixed(2);
  }

  /**
   * on change sheet data
   */
  onChangeSheetData(event: any) {
    if (!event) {
      return;
    }
    const val = event;
    this.isDataLoaded = false;
    this.liteBillService
      .loadData(this.workBook, this.convertedData, val)
      .then((data: any) => {
        let { convertedData, colums, lastLiteBillData } = data;
        debugger;
        this.convertedData = convertedData;
        this.lastLiteBillData = lastLiteBillData;

        this.colums = colums;
        this.currentPage = 1;
        this.page = 1;
        this.arrangeData(convertedData);
        this.isDataLoaded = true;
      })
      .catch((err: any) => {
        this.isDataLoaded = true;
      });
    // this.selectedWorkSheetName = this.workBook.SheetNames[val];
    // var worksheet = this.workBook.Sheets[this.selectedWorkSheetName];
  }

  /**
   * export file as excel
   */
  export(): void {
    // this.data.unshift(this.colums);
    // console.log(this.data);

    this.liteBillService.export(this.workBook);
  }

  /**
   * get Data From Local File
   */
  getDataOfLocalFile() {
    this.isDataLoaded = false;
    this.data = [];
    this.liteBillService
      .getDataOfLocalFile()
      .then((res: any) => {
        if (JSON.stringify(res) != "{}") {
          let {
            selectedWorkSheet,
            selectedWorkSheetName,
            workBook,
            convertedData,
            selectedWorkSheetNamesArray,
            worksSheets,
            colums,
            lastLiteBillData,
          } = res;
          this.selectedWorkSheet = selectedWorkSheet;
          this.selectedWorkSheetName = selectedWorkSheetName;
          this.workBook = workBook;
          this.convertedData = convertedData;
          this.selectedWorkSheetNamesArray = selectedWorkSheetNamesArray;
          this.worksSheets = worksSheets;
          this.colums = colums;
          this.lastLiteBillData = lastLiteBillData;

          this.currentPage = 1;
          this.page = 1;
          this.arrangeData(convertedData);
          this.isDataLoaded = true;
          let fileSelector = <any>document.getElementById("file-select");
          fileSelector.value = "";
        }
      })
      .catch((err) => {
        this.isDataLoaded = true;
        throw new Error(err.message);
      });
  }

  getSelectedData(data: any) {
    // this.liteBillForm.patchValue({
    //   current_unit: data.current_unit,
    //   previouse_unit: data.current_unit,
    //   Used_Unit: data.Used_Unit,
    //   Extra_Current_Unit: data.Extra_Current_Unit,
    //   Extra_Previouse_Unit: data.Extra_Previouse_Unit,
    //   Extra_Used_Unit: data.Extra_Used_Unit,
    //   Unit_Point: data.Unit_Point,
    //   RS: data.RS,
    //   comments: data.comments,
    // });
  }

  /**
   * add new bill
   * @param newBillValue new bill amount
   * @returns if any error
   */
  addNewLiteBill() {
    this.lastLiteBillData = this.newLastBill;
    this.convertedData.push(this.newLastBill);
    console.log("new lite bill is ==>", this.newLastBill);
    this.newLastBill = this.reset();
    this.arrangeData(this.convertedData);
  }

  /**
   * Validation for numeric value
   */
  onlyNumericKey(event: any) {
    if (
      (event.keyCode >= 48 && event.keyCode <= 57) ||
      event.keyCode === 190 ||
      event.keyCode == 46
    ) {
      return true;
    } else {
      event.preventDefault();
      return false;
    }
  }

  readExcel() {
    let readFile = new FileReader();
    readFile.onload = (e) => {
      this.storeData = readFile.result;
      var data = new Uint8Array(this.storeData);
      var arr = new Array();
      for (var i = 0; i != data.length; ++i)
        arr[i] = String.fromCharCode(data[i]);
      var bstr = arr.join("");
      var workbook = XLSX.read(bstr, { type: "binary" });
      var first_sheet_name = workbook.SheetNames[0];
      this.worksheet = workbook.Sheets[first_sheet_name];
    };
    readFile.readAsArrayBuffer(this.fileUploaded);
  }

  readAsCSV() {
    this.csvData = XLSX.utils.sheet_to_csv(this.worksheet);
    const data: Blob = new Blob([this.csvData], {
      type: "text/csv;charset=utf-8;",
    });
    FileSaver.saveAs(data, "Exported_CSVFile" + new Date().getTime() + ".csv");
  }

  readAsJson() {
    this.jsonData = XLSX.utils.sheet_to_json(this.worksheet, { raw: false });
    this.jsonData = JSON.stringify(this.jsonData);
    const data: Blob = new Blob([this.jsonData], { type: "application/json" });
    FileSaver.saveAs(
      data,
      "Exported_JsonFile" + new Date().getTime() + ".json"
    );
  }

  readAsHTML() {
    this.htmlData = XLSX.utils.sheet_to_html(this.worksheet);
    const data: Blob = new Blob([this.htmlData], {
      type: "text/html;charset=utf-8;",
    });
    FileSaver.saveAs(
      data,
      "Exported_HtmlFile" + new Date().getTime() + ".html"
    );
  }

  readAsText() {
    this.textData = XLSX.utils.sheet_to_txt(this.worksheet);
    const data: Blob = new Blob([this.textData], {
      type: "text/plain;charset=utf-8;",
    });
    FileSaver.saveAs(data, "Exported_TextFile" + new Date().getTime() + ".txt");
  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }

  arrangeData(data: any) {
    if (data.length >= 1) {
      this.totalRecords = data.length;
      if (this.totalRecords >= 10) {
        this.pages = new Array(Math.ceil(this.totalRecords / 10));
      } else {
        this.pages = new Array(1);
      }
      this.isOpenAddMenu = false;
      this.maxSize = this.page * 10;
      this.minSize = this.maxSize - 10;
      let list = data.slice(this.minSize, this.maxSize);
      this.displayData = [];
      this.displayData = list;
    }
  }

  /**
   * set the paging with data as per user clicks on page numbers
   * @param i
   * @param event
   */
  public setPage(i: number, event: any): void {
    event.preventDefault();
    this.page = i;
    this.currentPage = i;
    // this.arrangeData(this.data);
    this.arrangeData(this.convertedData);
  }

  // go to previous page as user clicks on the previus button
  public previous(): void {
    this.page = this.currentPage - 1;
    this.currentPage = this.page;
    // this.arrangeData(this.data);
    this.arrangeData(this.convertedData);
  }

  //  go to next page as user clicks the next button
  public next(): void {
    this.page = this.currentPage + 1;
    this.currentPage = this.page;
    // this.arrangeData(this.data);
    this.arrangeData(this.convertedData);
  }

  // go to the last page
  public gotoLastPage(): void {
    this.page = this.currentPage = this.pages.length;
    // this.arrangeData(this.data);
    this.arrangeData(this.convertedData);
  }

  // go to the first page
  public gotoFisrtPage(): void {
    this.page = this.currentPage = 1;
    // this.arrangeData(this.data);
    this.arrangeData(this.convertedData);
  }
}

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from "@angular/common/http";


// material declaration
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { UpdateBillComponent } from './update-bill/update-bill.component';
// import { MatButtonModule } from '@angular/material/button';
// import { MatMenuModule } from '@angular/material/menu';
// import { MatToolbarModule } from '@angular/material/toolbar';
// import { MatIconModule } from '@angular/material/icon';
// import { MatCardModule } from '@angular/material/card';
// import { MatFormFieldModule } from '@angular/material/form-field';
// import { MatInputModule } from '@angular/material/input';
// import { MatStepperModule } from '@angular/material/stepper';
// import { MatListModule } from '@angular/material/list';
// import { MatSidenavModule } from '@angular/material/sidenav';
// import { MatCheckboxModule } from '@angular/material/checkbox';
// import { MatRadioModule } from '@angular/material/radio';
// import { MaterialFormComponent } from './Angular Material/material-form/material-form.component';

import { DataTablesModule } from "angular-datatables";
import { AppPaginationComponent } from './app-pagination/app-pagination.component';
import { FilterDataPipe } from './pipes/filter-data.pipe';
import { SampleExcelComponent } from './sample-excel/sample-excel.component';
import { DynamicExcelComponent } from './dynamic-excel/dynamic-excel.component';

@NgModule({
  declarations: [
    AppComponent,
    UpdateBillComponent,
    AppPaginationComponent,
    FilterDataPipe,
    SampleExcelComponent,
    DynamicExcelComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    DataTablesModule,
    // MatButtonModule,
    // MatMenuModule,
    // MatToolbarModule,
    // MatIconModule,
    // MatCardModule,
    // MatFormFieldModule,
    // MatInputModule,
    // MatStepperModule,
    // MatListModule,
    // MatSidenavModule,
    // MatCheckboxModule,
    // MatRadioModule,
  ],
  exports:[
    AppPaginationComponent
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {
  constructor() {
    // console.log('app module loaded');
  }
}

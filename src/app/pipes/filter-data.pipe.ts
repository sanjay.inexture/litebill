import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: "filterData",
})
export class FilterDataPipe implements PipeTransform {
  /**
   * convert to number
   * @param x value
   * @returns
   */
  transform(value: any, ...args: any[]): unknown {
    // debugger;
    let fieldName = args[0];
    if (!value) {
      return;
    }
    if (typeof value === 'object' && Object.prototype.toString.call(value) === '[object Date]') {
      var today = value;
      var dd = String(today.getDate()).padStart(2, "0");
      var mm = String(today.getMonth() + 1).padStart(2, "0"); //January is 0!
      var yyyy = today.getFullYear();

      return dd + "/" + mm + "/" + yyyy;
    }
    if (typeof value == "number" && !isNaN(value)) {
      // check if it is integer
      if (fieldName === "point") {
        Number(value);
        return;
      }
      if (Number.isInteger(value)) {
        // console.log(`${value} is integer.`);
        return Number(value).toFixed(2);
      } else {
        // console.log(`${value} is a float value.`);
        return value.toFixed(2);
      }
    } else {
      if (typeof value === "object") {
        return value;
      }
      if (value.startsWith("$")) {
        value = value.substring(1);
      }
      // console.log(`${value} is not a number`);
      return value;
    }
  }
}

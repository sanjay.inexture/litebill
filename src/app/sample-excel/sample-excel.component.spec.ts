import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SampleExcelComponent } from './sample-excel.component';

describe('SampleExcelComponent', () => {
  let component: SampleExcelComponent;
  let fixture: ComponentFixture<SampleExcelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SampleExcelComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SampleExcelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

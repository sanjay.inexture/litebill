import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs';
import * as XLSX from "xlsx";
import readXlsxFile from "read-excel-file";
import { FormBuilder, Validators } from "@angular/forms";
import * as FileSaver from "file-saver";

@Injectable({
  providedIn: "root",
})
export class GoogleSheetService {
  data: any = null;
  // create schema for filed name with type
  schema = {
    Date: {
      prop: "Date",
      type: Date,
    },
    Current_Unit: {
      prop: "Current_Unit",
      type: String,
    },
    Extra_Current_Unit: {
      prop: "Extra_Current_Unit",
      type: String,
    },
    Previouse_Unit: {
      prop: "Previouse_Unit",
      type: String,
    },
    Extra_Previouse_Unit: {
      prop: "Extra_Previouse_Unit",
      type: String,
    },
    Used_Unit: {
      prop: "Used_Unit",
      type: String,
    },
    Extra_Used_Unit: {
      prop: "Extra_Used_Unit",
      type: String,
    },
    Unit_Point: {
      prop: "Unit_Point",
      type: String,
    },
    RS: {
      prop: "RS",
      type: String,
    },
    comments: {
      prop: "comments",
      type: String,
    },
  };
  EXCEL_TYPE =
    "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8";
  EXCEL_EXTENSION = ".xlsx";
  constructor(private http: HttpClient) {}

  load(id: any) {
    if (this.data) {
      // already loaded data
      return Promise.resolve(this.data);
    }
    const sheetid =
      "1vQmcGI6O83y0H7NzuEvTDPBqFN5_3J7ZGYpYCOxc26sFJTy8fJN-0SNI0VuFpBVAJIsIl38SIkEqulW";
    // let url =
    //   `https://spreadsheets.google.com/feeds/list/${id}/2PACX/public/values?alt=json`;
    var url =
      "https://spreadsheets.google.com/feeds/list/" +
      id +
      "/od6/public/values?alt=json";
    return new Promise((resolve) => {
      // We're using Angular Http provider to request the data,
      // then on the response it'll map the JSON data to a parsed JS object.
      // Next we process the data and resolve the promise with the new data.
      this.http
        .get(url)
        .pipe(map((res: any) => res.json()))
        .subscribe((data: any) => {
          debugger;
          console.log("Raw Data", data);
          this.data = data.feed.entry;

          let returnArray: Array<any> = [];
          if (this.data && this.data.length > 0) {
            this.data.forEach((entry: any, index: any) => {
              var obj = {} as any;
              for (let x in entry) {
                if (x.includes("gsx$") && entry[x].$t) {
                  obj[x.split("$")[1]] = entry[x]["$t"];
                  // console.log( x.split('$')[1] + ': ' + entry[x]['$t'] );
                }
              }
              returnArray.push(obj);
            });
          }
          resolve(returnArray);
        });
    });
  }

  // getSheetData() {
  //   const sheetid =
  //     "1vQmcGI6O83y0H7NzuEvTDPBqFN5_3J7ZGYpYCOxc26sFJTy8fJN-0SNI0VuFpBVAJIsIl38SIkEqulW";
  //   const sheetno = "0";
  //   // https://docs.google.com/spreadsheets/d/e/2PACX-1vQmcGI6O83y0H7NzuEvTDPBqFN5_3J7ZGYpYCOxc26sFJTy8fJN-0SNI0VuFpBVAJIsIl38SIkEqulW/pubhtml
  //   let url =
  //     //  "https://docs.google.com/spreadsheets/d/1wwIsyKDZz63KMM1bNwr1UXTZHonyRchS3DKfbfVdEHE";
  //     // "https://spreadsheets.google.com/feeds/worksheets/1wwIsyKDZz63KMM1bNwr1UXTZHonyRchS3DKfbfVdEHE/private/full";
  //     `https://spreadsheets.google.com/feeds/list/${sheetid}/2PACX/public/values?alt=json`;
  //   debugger;
  //   return this.http.get(url).pipe(
  //     map((res: any) => {
  //       const data = res.feed.entry;
  //       debugger;
  //       const returnArray: Array<any> = [];
  //       if (data && data.length > 0) {
  //         data.forEach((entry: any) => {
  //           const obj = {} as any;
  //           for (const x in entry) {
  //             if (x.includes("gsx$") && entry[x].$t) {
  //               obj[x.split("$")[1]] = entry[x]["$t"];
  //             }
  //           }
  //           returnArray.push(obj);
  //         });
  //       }
  //       debugger;
  //       return returnArray;
  //     })
  //   );
  // }

  /**
   * get selected excel file data
   * @param evt current tag event
   */
  getSelectedFileData(evt: any) {
    // /* wire up file reader */
    const target: DataTransfer = <DataTransfer>evt.target;
    if (target.files.length !== 1) throw new Error("Cannot use multiple files");
    // const reader: FileReader = new FileReader();
    // reader.onload = (e: any) => {
    //   /* read workbook */
    //   const bstr: string = e.target.result;
    //   const wb: XLSX.WorkBook = XLSX.read(bstr, { type: "binary" });

    //   /* grab first sheet */
    //   const wsname: string = wb.SheetNames[0];
    //   const ws: XLSX.WorkSheet = wb.Sheets[wsname];

    //   /* save data */
    //   this.data = <AOA>XLSX.utils.sheet_to_json(ws, { header: 1 });
    //   console.log(this.data);
    // };
    // reader.readAsBinaryString(target.files[0]);

    //for selected files
    const schema = this.schema;
    readXlsxFile(evt.target.files[0], { schema }).then(({ rows, errors }) => {
      if (rows.length > 0) {
        console.log("rows is ==>", rows);
        return rows as any;
      } else {
        alert("there is not any data available. please add atlease one data");
        return false;
      }
      // `errors` list items have shape: `{ row, column, error, value }`.
      errors.length === 0;
    });
  }

  /**
   * export file as excel
   */
  export(): void {
    //  let element = document.getElementById("excel-table");
    //  const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(element);

    //  /* generate workbook and add the worksheet */
    //  const wb: XLSX.WorkBook = XLSX.utils.book_new();
    //  XLSX.utils.book_append_sheet(wb, ws, "Sheet1");

    //  /* save to file */
    //  XLSX.writeFile(wb, this.fileName);
    /* generate worksheet */
    // const ws: XLSX.WorkSheet = XLSX.utils.aoa_to_sheet(this.data);

    // /* generate workbook and add the worksheet */
    // const wb: XLSX.WorkBook = XLSX.utils.book_new();
    // XLSX.utils.book_append_sheet(wb, ws, "Sheet1");

    // /* save to file */
    // XLSX.writeFile(wb, this.fileName);

    const myworksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(this.data);
    const myworkbook: XLSX.WorkBook = {
      Sheets: { data: myworksheet },
      SheetNames: ["data"],
    };
    const excelBuffer: any = XLSX.write(myworkbook, {
      bookType: "xlsx",
      type: "array",
    });
    this.saveAsExcelFile(excelBuffer, "litebill-copy.csv");
  }

  /**
   * save file as excel
   * @param buffer buffer data of sheet
   * @param fileName excel sheet name
   */
  saveAsExcelFile(buffer: any, fileName: string): void {
    const data: Blob = new Blob([buffer], {
      type: this.EXCEL_TYPE,
    });
    FileSaver.saveAs(data, fileName + "_exported" + this.EXCEL_EXTENSION);
  }

  /**
   * get Data From Local File
   */
  getDataOfLocalFile() {
    // // let fileReader = new FileReader();
    // // const files = "../assets/excel/litebill.xlsx";
    // // fileReader.readAsArrayBuffer(files);
    // // fetch(file)
    // //   .then((response) =>
    // //    response.arrayBuffer())
    // //   .then((data) => {
    // //     // Do something with your data
    // //     const binarystr = data;
    // //     const wb: XLSX.WorkBook = XLSX.read(binarystr, { type: "binary" });

    // //     /* selected the first sheet */
    // //     const wsname: string = wb.SheetNames[0];
    // //     const ws: XLSX.WorkSheet = wb.Sheets[wsname];

    // //     /* save data */
    // //     const result = XLSX.utils.sheet_to_json(ws); // to get 2d array pass 2nd parameter as object {header: 1}
    // //     console.log(result); // Data will be logged in array format containing objects
    // //   });

    // create schema for filed name with type
    this.http
      .get("assets/excel/litebill.xlsx", { responseType: "blob" })
      .subscribe((data: any) => {
        const reader: FileReader = new FileReader();

        reader.onload = (e: any) => {
          // using local file with readXlsxFile
          const file = e.target.result;
          const schema = this.schema;
          readXlsxFile(file, { schema }).then(({ rows, errors }) => {
            if (rows.length > 0) {
              console.log("rows is ==>", rows);
              return rows as any;
            } else {
              alert(
                "there is not any data available. please add atlease one data"
              );
              return false;
            }
            // `errors` list items have shape: `{ row, column, error, value }`.
            errors.length === 0;
          });

          // using local file with XLSX

          // const bstr: string = e.target.result;
          // const wb: XLSX.WorkBook = XLSX.read(bstr, { type: "binary" });

          // /* grab first sheet */
          // const wsname1: string = wb.SheetNames[1];
          // const ws1: XLSX.WorkSheet = wb.Sheets[wsname1];

          // /* grab second sheet */
          // const wsname2: string = wb.SheetNames[2];
          // const ws2: XLSX.WorkSheet = wb.Sheets[wsname2];

          // /* save data */
          // dataJson1 = XLSX.utils.sheet_to_json(ws1);
          // dataJson2 = XLSX.utils.sheet_to_json(ws2);
          // console.log(dataJson1);
          // this.data = dataJson1;
        };
        reader.readAsBinaryString(data);
        console.log(data);
      });
  }

  getSelectedData(data: any) {
    // this.liteBillForm.patchValue({
    //   current_unit: data.current_unit,
    //   previouse_unit: data.current_unit,
    //   Used_Unit: data.Used_Unit,
    //   Extra_Current_Unit: data.Extra_Current_Unit,
    //   Extra_Previouse_Unit: data.Extra_Previouse_Unit,
    //   Extra_Used_Unit: data.Extra_Used_Unit,
    //   Unit_Point: data.Unit_Point,
    //   RS: data.RS,
    //   comments: data.comments,
    // });
  }
}

import { TestBed } from '@angular/core/testing';

import { LiteBillService } from './lite-bill.service';

describe('LiteBillService', () => {
  let service: LiteBillService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LiteBillService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

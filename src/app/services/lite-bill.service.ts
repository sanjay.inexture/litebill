import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { map } from "rxjs";
import * as XLSX from "xlsx";
import readXlsxFile from "read-excel-file";
import * as FileSaver from "file-saver";

@Injectable({
  providedIn: "root",
})
export class LiteBillService {
  data: any = null;
  // create schema for filed name with type
  schema = {
    Date: {
      prop: "Date",
      type: Date,
    },
    Current_Unit: {
      prop: "Current_Unit",
      type: String,
    },
    Extra_Current_Unit: {
      prop: "Extra_Current_Unit",
      type: String,
    },
    Previouse_Unit: {
      prop: "Previouse_Unit",
      type: String,
    },
    Extra_Previouse_Unit: {
      prop: "Extra_Previouse_Unit",
      type: String,
    },
    Used_Unit: {
      prop: "Used_Unit",
      type: String,
    },
    Extra_Used_Unit: {
      prop: "Extra_Used_Unit",
      type: String,
    },
    Unit_Point: {
      prop: "Unit_Point",
      type: String,
    },
    RS: {
      prop: "RS",
      type: String,
    },
    comments: {
      prop: "comments",
      type: String,
    },
  };
  EXCEL_TYPE =
    "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8";
  EXCEL_EXTENSION = ".xlsx";
  selectedWorkSheet: any;
  selectedWorkSheetName: any;
  workBook: any;
  convertedData: any;
  selectedWorkSheetNamesArray: any = [];
  worksSheets: any = [];
  colums: any;
  lastLiteBillData: any;
  outputData: any = [];
  constructor(private http: HttpClient) {}

  load(id: any) {
    if (this.data) {
      // already loaded data
      return Promise.resolve(this.data);
    }
    const sheetid =
      "1vQmcGI6O83y0H7NzuEvTDPBqFN5_3J7ZGYpYCOxc26sFJTy8fJN-0SNI0VuFpBVAJIsIl38SIkEqulW";
    // let url =
    //   `https://spreadsheets.google.com/feeds/list/${id}/2PACX/public/values?alt=json`;
    var url =
      "https://spreadsheets.google.com/feeds/list/" +
      id +
      "/od6/public/values?alt=json";
    return new Promise((resolve) => {
      // We're using Angular Http provider to request the data,
      // then on the response it'll map the JSON data to a parsed JS object.
      // Next we process the data and resolve the promise with the new data.
      this.http
        .get(url)
        .pipe(map((res: any) => res.json()))
        .subscribe((data: any) => {
          // debugger;
          console.log("Raw Data", data);
          this.data = data.feed.entry;

          let returnArray: Array<any> = [];
          if (this.data && this.data.length > 0) {
            this.data.forEach((entry: any, index: any) => {
              var obj = {} as any;
              for (let x in entry) {
                if (x.includes("gsx$") && entry[x].$t) {
                  obj[x.split("$")[1]] = entry[x]["$t"];
                  // console.log( x.split('$')[1] + ': ' + entry[x]['$t'] );
                }
              }
              returnArray.push(obj);
            });
          }
          resolve(returnArray);
        });
    });
  }

  // getSheetData() {
  //   const sheetid =
  //     "1vQmcGI6O83y0H7NzuEvTDPBqFN5_3J7ZGYpYCOxc26sFJTy8fJN-0SNI0VuFpBVAJIsIl38SIkEqulW";
  //   const sheetno = "0";
  //   // https://docs.google.com/spreadsheets/d/e/2PACX-1vQmcGI6O83y0H7NzuEvTDPBqFN5_3J7ZGYpYCOxc26sFJTy8fJN-0SNI0VuFpBVAJIsIl38SIkEqulW/pubhtml
  //   let url =
  //     //  "https://docs.google.com/spreadsheets/d/1wwIsyKDZz63KMM1bNwr1UXTZHonyRchS3DKfbfVdEHE";
  //     // "https://spreadsheets.google.com/feeds/worksheets/1wwIsyKDZz63KMM1bNwr1UXTZHonyRchS3DKfbfVdEHE/private/full";
  //     `https://spreadsheets.google.com/feeds/list/${sheetid}/2PACX/public/values?alt=json`;
  //   debugger;
  //   return this.http.get(url).pipe(
  //     map((res: any) => {
  //       const data = res.feed.entry;
  //       debugger;
  //       const returnArray: Array<any> = [];
  //       if (data && data.length > 0) {
  //         data.forEach((entry: any) => {
  //           const obj = {} as any;
  //           for (const x in entry) {
  //             if (x.includes("gsx$") && entry[x].$t) {
  //               obj[x.split("$")[1]] = entry[x]["$t"];
  //             }
  //           }
  //           returnArray.push(obj);
  //         });
  //       }
  //       debugger;
  //       return returnArray;
  //     })
  //   );
  // }

  /**
   * get selected excel file data
   * @param evt current tag event
   */
  getSelectedFileData(evt: any) {
    // /* wire up file reader */
    const target: DataTransfer = <DataTransfer>evt.target;
    if (target.files.length !== 1) throw new Error("Cannot use multiple files");
    return new Promise((res, rej) => {
      let fileReader = new FileReader();
      fileReader.readAsArrayBuffer(evt.target.files[0]);
      fileReader.onload = (e) => {
        let arrayBuffer = fileReader.result as any;
        var data = new Uint8Array(arrayBuffer);
        var arr = new Array();
        for (var i = 0; i != data.length; ++i)
          arr[i] = String.fromCharCode(data[i]);
        var bstr = arr.join("");
        var workbook = XLSX.read(bstr, {
          type: "binary",
          cellDates: true,
          cellNF: false,
          cellText: false,
        });
        var first_sheet_name = workbook.SheetNames[0];
        var worksheet = workbook.Sheets[first_sheet_name];
        // var arraylist = XLSX.utils.sheet_to_json(worksheet, {
        //   dateNF: "dd/mm/yyyy",
        // });
        // console.log("arrayList dateNF: 'dd/mm/yyyy'  =>", arraylist);
        // var arraylist = XLSX.utils.sheet_to_json(worksheet, {
        //   defval: "",
        //   raw : false
        // });
        // console.log("arrayList defval: ''  =>", arraylist);
        // var arraylist = XLSX.utils.sheet_to_json(worksheet, {
        //   raw: false,
        // });
        // console.log("arrayList raw: false  =>", arraylist);
        // var arraylist = XLSX.utils.sheet_to_json(worksheet, {
        //   header: 1,
        // });
        // console.log("arrayList header: 1 =>", arraylist);
        // var arraylist = XLSX.utils.sheet_to_json(worksheet, {
        //   raw: false,
        //   header: 1,
        //   // defval: "",
        // });
        // console.log("arrayList raw: false, header: 1, =>", arraylist);
        // var arraylist = XLSX.utils.sheet_to_json(worksheet, {
        //   raw: true,
        //   header: 1,
        //   // defval: "",
        // });
        // console.log("arrayList raw: true, header: 1, =>", arraylist);
        // var arraylist = XLSX.utils.sheet_to_json(worksheet, {
        //   raw: false,
        //   header: 1,
        //   defval: "",
        // });
        // console.log("arrayList raw: false, header: 1,defval: '' =>", arraylist);
        var arraylist = XLSX.utils.sheet_to_json(worksheet, {
          raw: false,
          dateNF: "dd/mm/yyyy",
          // header: "A",
          defval: "",
        });
        console.log("arrayList raw: true,defval: '' =>", arraylist);
        //  this.filelist = [];
        // console.log(arraylist);
        this.workBook = workbook;
        this.outputData = arraylist;
        // const result = {
        //   data: arraylist,
        //   workbook: workbook,
        // };
        if (arraylist.length > 0) {
          //  console.log("rows is ==>", arraylist);
          this.loadData(workbook, arraylist)
            .then((data: any) => {
              res(data);
            })
            .catch((err) => {
              alert(
                "there is not any data available. please add atlease one data"
              );
              rej(new Error("no data found on selected excel file"));
            });
        } else {
          alert("there is not any data available. please add atlease one data");
          rej(new Error("no data found on selected excel file"));
        }
      };
    });

    //for selected files using readXlsxFile

    // return new Promise((res, rej) => {
    //   const schema = this.schema;
    //   readXlsxFile(evt.target.files[0], { schema }).then(({ rows, errors }) => {
    //     if (rows.length > 0) {
    //       console.log("rows is ==>", rows);
    //       res(rows);
    //     } else {
    //       alert("there is not any data available. please add atlease one data");
    //       rej(new Error("no data found on selected excel file"));
    //     }
    //     // `errors` list items have shape: `{ row, column, error, value }`.
    //     errors.length === 0;
    //   });
    // });
  }

  /**
   * convert data json to sheet
   */
  convertJsonToSheet(workBook : any) {
    let wb = workBook;
    let fetchedList = Object.assign(wb.Sheets, {});
    Object.keys(fetchedList).filter((data: any) => {
      let selectedWorkSheet = fetchedList[data];
      let newSheet = XLSX.utils.json_to_sheet(selectedWorkSheet);
      fetchedList[data] = newSheet;
    });
    console.log("convertJsonToSheet list ==>", fetchedList);
    this.export(wb);
  }

  /**
   * export file as excel
   */
  export(wb: any): void {
    //  let element = document.getElementById("excel-table");
    //  const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(element);

    //  /* generate workbook and add the worksheet */
    //  const wb: XLSX.WorkBook = XLSX.utils.book_new();
    //  XLSX.utils.book_append_sheet(wb, ws, "Sheet1");

    //  /* save to file */
    //  XLSX.writeFile(wb, this.fileName);
    /* generate worksheet */
    // const ws: XLSX.WorkSheet = XLSX.utils.aoa_to_sheet(this.data);

    // /* generate workbook and add the worksheet */
    // const wb: XLSX.WorkBook = XLSX.utils.book_new();
    // XLSX.utils.book_append_sheet(wb, ws, "Sheet1");

    // /* save to file */
    // XLSX.writeFile(wb, this.fileName);

    // const myworksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(data);
    // const myworkbook: XLSX.WorkBook = {
    //   Sheets: { data: myworksheet },
    //   SheetNames: ["data"],
    // };
    const excelBuffer: any = XLSX.write(wb, {
      bookType: "xlsx",
      type: "array",
    });
    this.saveAsExcelFile(excelBuffer, "litebill");
  }

  /**
   * save file as excel
   * @param buffer buffer data of sheet
   * @param fileName excel sheet name
   */
  saveAsExcelFile(buffer: any, fileName: string): void {
    const data: Blob = new Blob([buffer], {
      type: this.EXCEL_TYPE,
    });
    FileSaver.saveAs(
      data,
      fileName + "_export_" + new Date().getTime() + this.EXCEL_EXTENSION
    );
  }

  /**
   * get Data From Local File
   */
  getDataOfLocalFile() {
    // // let fileReader = new FileReader();
    // // const files = "../assets/excel/litebill.xlsx";
    // // fileReader.readAsArrayBuffer(files);
    // // fetch(file)
    // //   .then((response) =>
    // //    response.arrayBuffer())
    // //   .then((data) => {
    // //     // Do something with your data
    // //     const binarystr = data;
    // //     const wb: XLSX.WorkBook = XLSX.read(binarystr, { type: "binary" });

    // //     /* selected the first sheet */
    // //     const wsname: string = wb.SheetNames[0];
    // //     const ws: XLSX.WorkSheet = wb.Sheets[wsname];

    // //     /* save data */
    // //     const result = XLSX.utils.sheet_to_json(ws); // to get 2d array pass 2nd parameter as object {header: 1}
    // //     console.log(result); // Data will be logged in array format containing objects
    // //   });

    // create schema for filed name with type
    return new Promise((res, rej) => {
      this.http
        .get("assets/excel/litebill.xlsx", { responseType: "blob" })
        .subscribe((data: any) => {
          const reader: FileReader = new FileReader();
          reader.onload = (e: any) => {
            // // using local file with readXlsxFile

            // const file = e.target.result;
            // const schema = this.schema;
            // readXlsxFile(file, { schema }).then(({ rows, errors }) => {
            //   if (rows.length > 0) {
            //     console.log("rows is ==>", rows);
            //     res(rows);
            //   } else {
            //     alert(
            //       "there is not any data available. please add atlease one data"
            //     );
            //     rej(new Error("no data found on selected excel file"));
            //   }
            //   // `errors` list items have shape: `{ row, column, error, value }`.
            //   errors.length === 0;
            // });

            // using local file with XLSX

            const bstr: string = e.target.result;
            const workbook: XLSX.WorkBook = XLSX.read(bstr, {
              type: "binary",
              cellDates: true,
              cellNF: false,
              cellText: false,
            });

            /* grab first sheet */
            // const wsname1: string = workbook.SheetNames[1];
            // const ws1: XLSX.WorkSheet = workbook.Sheets[wsname1];

            // /* grab second sheet */
            // const wsname2: string = workbook.SheetNames[2];
            // const ws2: XLSX.WorkSheet = workbook.Sheets[wsname2];

            /* save data */
            // let arrayBuffer = reader.result as any;
            // var data = new Uint8Array(arrayBuffer);
            // var arr = new Array();
            // for (var i = 0; i != data.length; ++i)
            // arr[i] = String.fromCharCode(data[i]);
            // var bstr = arr.join("");
            // var workbook = XLSX.read(bstr, { type: "binary" });
            var first_sheet_name = workbook.SheetNames[0];
            var worksheet = workbook.Sheets[first_sheet_name];
            var arraylist = XLSX.utils.sheet_to_json(worksheet, {
              //  header: "Date" ? 0 : 1,
              // raw: false,
              dateNF: "dd/mm/yyyy",
              // defval: "",
            });
            //  this.filelist = [];
            // console.log(arraylist);
            this.workBook = workbook;
            this.outputData = arraylist;
            // const result = {
            //   data: arraylist,
            //   workbook: workbook,
            // };
            if (arraylist.length > 0) {
              // console.log("rows is ==>", arraylist);
              this.loadData(workbook, arraylist)
                .then((data) => {
                  res(data);
                })
                .catch((err) => {
                  alert(
                    "there is not any data available. please add atlease one data"
                  );
                  rej(new Error("no data found on selected excel file"));
                });
            } else {
              alert(
                "there is not any data available. please add atlease one data"
              );
              rej(new Error("no data found on selected excel file"));
            }
          };
          reader.readAsBinaryString(data);
          // console.log(data);
        });
    });
  }

  getSelectedData(data: any) {
    // this.liteBillForm.patchValue({
    //   current_unit: data.current_unit,
    //   previouse_unit: data.current_unit,
    //   Used_Unit: data.Used_Unit,
    //   Extra_Current_Unit: data.Extra_Current_Unit,
    //   Extra_Previouse_Unit: data.Extra_Previouse_Unit,
    //   Extra_Used_Unit: data.Extra_Used_Unit,
    //   Unit_Point: data.Unit_Point,
    //   RS: data.RS,
    //   comments: data.comments,
    // });
  }

  loadData(workBook: any, data: any, newSelectedVal?: any) {
    return new Promise((res, rej) => {
      try {
        // debugger;
        var worksheet: any;
        if (newSelectedVal) {
          this.selectedWorkSheet = newSelectedVal;
          this.selectedWorkSheetName = this.workBook.SheetNames[newSelectedVal];
          worksheet = this.workBook.Sheets[this.selectedWorkSheetName];
          this.convertedData = XLSX.utils.sheet_to_json(worksheet, {
            raw: false,
            dateNF: "dd/mm/yyyy",
            defval: "",
          });
        } else {
          this.workBook = workBook;
          this.convertedData = data;
          this.selectedWorkSheetNamesArray = [];
          this.selectedWorkSheetNamesArray = this.workBook.SheetNames;
          // console.log(
          //   "selectedWorkSheetNamesArray ==>",
          //   this.selectedWorkSheetNamesArray
          // );
          this.worksSheets = [];
          this.worksSheets = this.workBook.Sheets;
          // console.log("worksSheets ==>", this.worksSheets);
          this.selectedWorkSheet = 0;
          this.selectedWorkSheetName = this.workBook.SheetNames[0];
          worksheet = this.workBook.Sheets[this.selectedWorkSheetName];
        }
        this.colums = Object.keys(this.convertedData[0]);
        // let cols = data[0] as any;
        // this.colums = cols.filter(function (el: any) {
        //   return el != null;
        // });
        // this.colums = arraylist[0];
        // console.log("colums  =>", this.colums);
        // console.log("convertedData  =>", this.convertedData);
        // console.log(data);
        // data.splice(0, 1);
        // this.data = data;
        let length = this.convertedData.length - 1;
        this.lastLiteBillData = this.convertedData[length];
        if (newSelectedVal) {
          let objData = {
            convertedData: this.convertedData,
            colums: this.colums,
            lastLiteBillData: this.lastLiteBillData,
          };
          res(objData);
        } else {
          let objData = {
            selectedWorkSheet: this.selectedWorkSheet,
            selectedWorkSheetName: this.selectedWorkSheetName,
            workBook: this.workBook,
            convertedData: this.convertedData,
            selectedWorkSheetNamesArray: this.selectedWorkSheetNamesArray,
            worksSheets: this.worksSheets,
            colums: this.colums,
            lastLiteBillData: this.lastLiteBillData,
          };
          res(objData);
        }
      } catch (error) {
        alert("there is not any data available. please add atlease one data");
        rej(new Error("no data found on selected excel file"));
      }
    });
  }
}

